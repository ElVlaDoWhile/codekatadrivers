# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This application helps to record drivers, saves the history of trips and generates reports according to the miles traveled and the time per trip
* 1.0.0

### How do I get set up? ###

* To test this application you can go to the directory "codekatadrivers/CodeKataDriverApp/bin/Debug/netcoreapp3.1" and run the file "CodeKataDriverApp.exe"
* You can also open and run from any version of Visual Studio

### Who do I talk to? ###

* thecodingbreak@gmail.com