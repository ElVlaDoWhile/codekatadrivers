﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CodeKataDriverApp
{
    class Program
    {
        /// <summary>
        /// These are the collections that are used during the execution of the application, in each one the drivers, travel history and trips with average speed are saved
        /// </summary>
        private static ObservableCollection<Driver> Drivers = new ObservableCollection<Driver>();
        private static ObservableCollection<Trip> Trips = new ObservableCollection<Trip>();
        private static ObservableCollection<TotalTrip> TotalTrips = new ObservableCollection<TotalTrip>();

        /// <summary>
        /// This is the main method that executes the menu to select an option
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Menu();
        }

        /// <summary>
        /// This method shows the menu and executes the selected option        
        /// /// </summary>
        private static void Menu()
        {
            Console.WriteLine("Please select one of the options:");
            Console.WriteLine("1.- Add new driver");
            Console.WriteLine("2.- Add new trip");
            Console.WriteLine("3.- View drivers");
            Console.WriteLine("4.- View trips");
            Console.WriteLine("5.- View total trips average");
            switch (Console.ReadLine())
            {
                case "1":
                    AddNewDriver();
                    break;
                case "2":
                    AddNewTrip();
                    break;
                case "3":
                    ShowDriversList();
                    Menu();
                    break;
                case "4":
                    ShowTripsList();
                    break;
                case "5":
                    ShowTotalTrips();
                    break;
                default:
                    Console.WriteLine("Please select a valid option");
                    Menu();
                    break;
            }
        }

        /// <summary>
        /// This method helps to add a new driver to drivers list
        /// </summary>
        private static void AddNewDriver()
        {
            Console.WriteLine("Enter the full name of the driver:");
            Driver newDriver = new Driver()
            {
                driverId = Drivers.Count > 0 ? Drivers.Count + 1 : 1,
                driverName = Console.ReadLine()
            };
            Drivers.Add(newDriver);
            PrintSeparator();
            Console.WriteLine("Driver added successfuly");
            PrintSeparator();
            Menu();
        }

        /// <summary>
        /// This method save a new trip and asks the user for the drivers name, the start time, the stop time and the miles traveled
        /// </summary>
        private static void AddNewTrip()
        {
            try
            {
                if (Drivers.Count == 0)
                {
                    PrintSeparator();
                    Console.WriteLine("Please add a new driver to be able to add a trip");
                    PrintSeparator();
                    Menu();
                    return;
                }
                ShowDriversList();
                Console.WriteLine("Please select one of the drivers:");
                int driverIdTemp = Int32.Parse(Console.ReadLine());
                Driver driverNameTemp = Drivers.Where(x => x.driverId == driverIdTemp).FirstOrDefault();
                Console.WriteLine("Please enter the start time of the trip (HH:mm):");
                DateTime startTimeTemp = DateTime.ParseExact(Console.ReadLine(), "HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                Console.WriteLine("Please enter the end time of the trip (HH:mm):");
                DateTime endTimeTemp = DateTime.ParseExact(Console.ReadLine(), "HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                Console.WriteLine("Please enter the miles of the trip:");
                var milesDrivenTemp = Double.Parse(Console.ReadLine());
                if (startTimeTemp > endTimeTemp)
                {
                    PrintSeparator();
                    Console.WriteLine("The start time must not be less than the stop time");
                    PrintSeparator();
                    AddNewTrip();
                    return;
                }
                var tempTrip = new Trip()
                {
                    tripId = Trips.Count > 0 ? Trips.Count + 1 : 1,
                    driverId = driverIdTemp,
                    driverName = driverNameTemp.driverName,
                    startTime = startTimeTemp,
                    stopTime = endTimeTemp,
                    milesDriven = milesDrivenTemp
                };
                Trips.Add(tempTrip); 
                PrintSeparator();
                Console.WriteLine("Trip added successfuly!!!");
                PrintSeparator();
                Menu();
            }
            catch (Exception ex)
            {
                PrintSeparator();
                Console.WriteLine("The information is incorrect, please try again.");
                PrintSeparator();
                AddNewTrip();
            }
        }

        /// <summary>
        /// This method shows the drivers list
        /// </summary>
        private static void ShowDriversList()
        {
            foreach (var item in Drivers)
            {
                PrintSeparator();
                Console.WriteLine(item.driverId + ".- " + item.driverName);
            }
            PrintSeparator();
        }

        /// <summary>
        /// This method shows the trips list
        /// </summary>
        private static void ShowTripsList()
        {
            foreach (var item in Trips)
            {
                PrintSeparator();
                Console.WriteLine(item.driverName + " | " + item.startTime + " | " + item.stopTime + " | " + item.milesDriven);
            }
            PrintSeparator();
            Menu();
        }

        /// <summary>
        /// This method shows the list of trips and calculates for each of the drivers their total miles traveled and averages the speed of each driver
        /// </summary>
        private static void ShowTotalTrips()
        {
            foreach (var item in Drivers)
            {
                double totalTripsHours = 0;
                double totalTripsMiles = 0;
                var totalTrips = Trips.Where(x => x.driverId == item.driverId).ToList();
                foreach (var item2 in totalTrips)
                {
                    totalTripsHours = totalTripsHours + (item2.stopTime - item2.startTime).TotalHours;
                    totalTripsMiles = totalTripsMiles + item2.milesDriven;
                }
                TotalTrip totalTrip = new TotalTrip()
                {
                    averageSpeed = CalculateAverageSpeed(totalTripsHours, totalTripsMiles),
                    driverId = item.driverId,
                    driverName = item.driverName,
                    milesDriven = Math.Round(totalTripsMiles, MidpointRounding.ToEven),
                    tripId = Trips.Count > 0 ? Trips.Count + 1 : 1
                };
                TotalTrips.Add(totalTrip);
            }
            var FinalList = TotalTrips.OrderByDescending(x => x.milesDriven).ToList();
            foreach (var item3 in FinalList)
            {
                if (!(item3.averageSpeed < 5 || item3.averageSpeed > 100))
                {
                    if (item3.milesDriven > 0)
                    {
                        Console.WriteLine(item3.driverName + ": " + item3.milesDriven + " @ " + item3.averageSpeed + " mph");
                    }
                    else
                    {
                        Console.WriteLine(item3.driverName + ": 0 miles");
                    }
                }
            }
            Menu();
        }

        /// <summary>
        /// This method calculates the speed average with two parameters the total hours and the total miles driven
        /// </summary>
        /// <param name="totalHours"></param>
        /// <param name="totalMilesDriven"></param>
        /// <returns></returns>
        private static double CalculateAverageSpeed(double totalHours, double totalMilesDriven)
        {
            return Math.Round((totalMilesDriven / totalHours), MidpointRounding.ToEven);
        }

        /// <summary>
        /// This method prints a line of asterisks
        /// </summary>
        private static void PrintSeparator()
        {
            Console.WriteLine("***********************************************************************************");
        }
    }

    /// <summary>
    /// This class is used for user registration
    /// </summary>
    class Driver
    {
        public int driverId { get; set; }
        public string driverName { get; set; }
    }

    /// <summary>
    /// This class is used for trips registration
    /// </summary>
    class Trip
    {
        public int tripId { get; set; }
        public int driverId { get; set; }
        public string driverName { get; set; }
        public DateTime startTime { get; set; }
        public DateTime stopTime { get; set; }
        public double milesDriven { get; set; }
    }

    /// <summary>
    /// This class is used for filtered trips registration
    /// </summary>
    class TotalTrip
    {
        public int tripId { get; set; }
        public int driverId { get; set; }
        public string driverName { get; set; }
        public double milesDriven { get; set; }
        public double averageSpeed { get; set; }
    }
}
